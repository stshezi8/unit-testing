package unitTest.com.unitTest.com;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

//@TestMethodOrder(MethodOrderer.Random.class) // This will run the test Randomly
//@TestMethodOrder(MethodOrderer.MethodName.class) // This will run the test order by Method name
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // This will run the test base on the order we specified
public class RandomOrderTest {
    @Order(2)
    @Test
    void testA(){System.out.println("Run Test A");}
    @Order(1)
    @Test
    void testB(){System.out.println("Run Test B");}
    @Order(3)
    @Test
    void testC(){System.out.println("Run Test C");}
    @Order(4)
    @Test
    void testD(){System.out.println("Run Test D");}
}
