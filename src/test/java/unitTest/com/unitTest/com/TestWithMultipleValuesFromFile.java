package unitTest.com.unitTest.com;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;
import unitTest.com.unitTest.com.calculate.Calculate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TestWithMultipleValuesFromFile {

    @ParameterizedTest
    @CsvFileSource(resources = "/integerSubtraction.csv")
    void integerSubtraction(int minuend, int subtrahend, int expectedResult){
        // SETUP
        Calculate calculate = new Calculate();

        // TEST
        int actualResult = calculate.integerSubtraction(minuend,subtrahend);
        //VERIFY
        assertEquals(expectedResult, actualResult);
    }
}
