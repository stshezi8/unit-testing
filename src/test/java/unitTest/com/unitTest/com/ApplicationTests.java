package unitTest.com.unitTest.com;

import org.junit.jupiter.api.*;
import org.springframework.boot.test.context.SpringBootTest;
import unitTest.com.unitTest.com.calculate.Calculate;

import javax.swing.text.Style;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
// @DisplayName = TO MAKE TEST METHODS EASY TO READ
@DisplayName("Calculation test")
class ApplicationTests {
Calculate calculate;
//This method will run before all methods . Maybe if you want to create database that will be used by all your tests
@BeforeAll
static void setup(){
	System.out.println("Run @BeforeAll");
}

// This method will run after all test finished . maybe to delete database after testing
@AfterAll
static void cleanup(){
	System.out.println("Run @AfterAll ");
}

// This method will run before each test
	@BeforeEach
	void initialisingObjects(){
		calculate = new Calculate();
	System.out.println("Run @BeforeEach");
	}

	// This method will run after each test
	@AfterEach
	void deleteObjects(){
	System.out.println("Run @AfterEach");
	}

	//Name and conversion
	// test_<Method we are testing>_<Condition we are testing>_<Expected results >
	@Test
	@DisplayName("4/4 = 2")
	void testIntegerDivision_WhenFourDividedByTwo_ShouldBeTwo() {
		// SETUP OR ARRANGE OR GIVEN
		Calculate divide = new Calculate();
		// TEST OR ACT OR WHEN
		int result = divide.integerDivision( 4, 2);
		// VERIFY OR ASSERT OR THEN
		assertEquals(2,result, "4/2 didn't produced 2" );
		System.out.println("testIntegerDivision_WhenFourDividedByTwo_ShouldBeTwo");
	}

	@Test
	@DisplayName("9-2 = 7")
	void subtractionMethod(){
		// SETUP
		Calculate subtract = new Calculate();
		int num1 =9;
		int num2 =2;
		int expected = 7;
		//TEST
		int result = subtract.integerSubtraction(num1,num2);
		//VERIFY
		// the 3rd parameter () -> num1 + "-" + num2 + "did not produced" + expected : if it's a lambda
		// like this will not get executed if the result passed only executed if the result fails
		assertEquals(expected,result, () -> num1 + "-" + num2 + "did not produced" + expected);
		System.out.println("subtractionMethod");
	}
	@Test
	void testIntegerDivision_WhenDividedByZero_ShouldThrowArithmeticException(){
	//SETUP
		int num1 = 9;
		int num2 = 0 ;
		String expected = "/ by zero";

	//Test
		ArithmeticException actualException = assertThrows(ArithmeticException.class, ()->{
			calculate.integerDivision(num1, num2);
		});

	//Verify
		assertEquals(expected,actualException.getMessage());

	}

}
