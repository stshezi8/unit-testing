package unitTest.com.unitTest.com;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import unitTest.com.unitTest.com.calculate.Calculate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class SCVTestExample {
    Calculate calculate;


    @ParameterizedTest
   @CsvSource({"33, 1,32", "54, 1,53", "24,1,23"})
    void integerSubtraction(int minuend, int subtrahend, int expectedResult){
        // SETUP
        Calculate calculate = new Calculate();

        // TEST
        int actualResult = calculate.integerSubtraction(minuend,subtrahend);
        //VERIFY
        assertEquals(expectedResult, actualResult);
    }

    // IF We want to test string parameters in this way
    // 1st Is 2 parameters , 2nd 1 Parameter and empty , 3rd is One Parameter and nul
    // @CsvSource({"apple, Orange", "Apple, ''", "Apple,"})
}
