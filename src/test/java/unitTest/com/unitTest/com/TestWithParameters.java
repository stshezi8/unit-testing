package unitTest.com.unitTest.com;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.boot.test.context.SpringBootTest;
import unitTest.com.unitTest.com.calculate.Calculate;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/*
* This Test will have parameters
* */
@SpringBootTest
public class TestWithParameters {
    Calculate calculate;
    @BeforeEach
    void setup(){
        calculate = new Calculate();
    }

    @ParameterizedTest
    @MethodSource("integerSubtractionInputParameters") // We can remove this name and make the bottom method name same as the name of this test
    void integerSubtraction(int minuend, int subtrahend, int expectedResult){
        // SETUP is arguments

        // TEST
        int actualResult = calculate.integerSubtraction(minuend,subtrahend);
        //VERIFY
        assertEquals(expectedResult, actualResult);
    }

    private static Stream<Arguments> integerSubtractionInputParameters(){
        return Stream.of(
                Arguments.of(33, 1,32),
                Arguments.of(54, 1,53),
                Arguments.of(24,1,23)
        );
    }

}
