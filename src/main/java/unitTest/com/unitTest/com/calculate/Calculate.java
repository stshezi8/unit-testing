package unitTest.com.unitTest.com.calculate;

public class Calculate {

    public int integerDivision(Integer num1, Integer num2) {
        return num1 / num2;
    }

    public int integerSubtraction(int minuend, int subtrahend) {
        return minuend - subtrahend;
    }
}
